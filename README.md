# Description

Configurations used regularly by the author. [Zimfw](https://github.com/zimfw/zimfw) is the plugin manager used in these configurations. Open `.zimrc` to view the plugins in use.


The author is not responsible for damage to your hardware, use at your own risk.

## Installation

Use the **curl** utility to download raw files.

```
# .ZIMRC
curl -o $HOME/.zimrc https://gitlab.com/CocoJamb0O/zsh-dotfiles/-/raw/main/.zimrc?ref_type=heads

# .ZSHRC
curl -o $HOME/.zshrc https://gitlab.com/CocoJamb0O/zsh-dotfiles/-/raw/main/.zshrc?ref_type=heads
```

You can also download configurations to `/etc/skel` so that newly created users can use these configurations. Use elevated privileges to download to `/etc/skel`.

```
# .ZIMRC
curl -o /etc/skel/.zimrc https://gitlab.com/CocoJamb0O/zsh-dotfiles/-/raw/main/.zimrc?ref_type=heads

# .ZSHRC
curl -o /etc/skel/.zshrc https://gitlab.com/CocoJamb0O/zsh-dotfiles/-/raw/main/.zshrc?ref_type=heads
```